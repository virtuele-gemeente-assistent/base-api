# Extend the official Rasa Core SDK image
FROM python:3.11-bullseye
RUN pip install --no-cache-dir \
    requests isodate websockets==12.0 python-socketio==5.10.0 python-engineio==4.8.0 \
    sanic[ext]==23.6.0 asyncio redis sentry_sdk==1.33.0 aiohttp==3.9.0 pytz elastic-apm==6.19.0 \
    yoyo-migrations
WORKDIR /usr/local/src/instructions
COPY ./src /usr/local/src/instructions
COPY ./src/instructions.py /usr/local/bin/instructions
RUN chmod +x /usr/local/bin/instructions
CMD [ "instructions"]

# aiofiles==23.2.1
# anyio==4.0.0
# certifi==2023.7.22
# h11==0.14.0
# html5tagger==1.3.0
# httpcore==0.18.0
# httptools==0.6.1
# httpx==0.25.0
# idna==3.4
# iniconfig==2.0.0
# multidict==6.0.4
# packaging==23.2
# pluggy==1.3.0
# pytest==7.4.3
# sanic==23.6.0
# sanic-routing==23.6.0
# sanic-testing==23.6.0
# sentry-sdk==1.33.0
# sniffio==1.3.0
# tracerite==1.1.0
# typing_extensions==4.8.0
# ujson==5.8.0
# urllib3==2.0.7
# uvloop==0.19.0
# websockets==12.0