#!/usr/local/bin/python
instructions = """
Goal is to create a simple yet effective Docker image with commands to be simply executed by:

docker run mycontainer (default command)

or

docker run mycontainer yoyo ...
docker run mycontainer init ...

To create this simply add your app to the filesystem in the following way, replacing myapp with the application name and mycmd with the entry python file:

WORKDIR /usr/local/src/[myapp]
COPY ./src /usr/local/src/[myapp]
COPY ./src/[mycmd].py /usr/local/bin/[mycmd]
RUN chmod +x /usr/local/bin/[mycmd]
CMD ["[mycmd]"]

multiple commands can be added by repeating lines 3+4, CMD defines default command to execute.

!!! make sure that every Pyhton file that must serve as command starts with:

#!/usr/local/bin/python

"""
print(instructions)